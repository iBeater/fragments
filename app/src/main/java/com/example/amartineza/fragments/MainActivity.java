package com.example.amartineza.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements EnviarMensaje, FragmentC.interfazRestar {
    FragmentA fragmentA;
    FragmentB fragmentB;
    FragmentC fragmentC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentA = fragmentA.newInstance();
        fragmentB = fragmentB.newInstance();
        fragmentC = fragmentC.newInstance();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_a, fragmentA, "Fragment A");
        fragmentTransaction.replace(R.id.frame_layout_b, fragmentB, "Fragment B");
        fragmentTransaction.replace(R.id.frame_layout_c, fragmentC, "Fragment C");
        fragmentTransaction.commit();

    }

    @Override
    public void enviarSaludoFragment(String mensaje, int contador, int id) {
        switch (id) {
            case 1:
                TextView textViewTempB = fragmentB.getView().findViewById(R.id.text_view_fragment_b);
                textViewTempB.setText(mensaje + " " + contador);
                break;
            case 2:
                TextView textViewTempA = fragmentA.getView().findViewById(R.id.text_view_fragment_a);
                textViewTempA.setText(mensaje + " " + contador);
                break;
        }
    }

    @Override
    public void restar() {
        fragmentA.setContadorFragmentA(fragmentA.getContadorFragmentA() - 1);
        TextView textViewTempA = fragmentA.getView().findViewById(R.id.text_view_fragment_a);
        textViewTempA.setText(fragmentA.getContadorFragmentA()+"");
        fragmentB.setContadorFragmentB(fragmentB.getContadorFragmentB() - 1);
        TextView textViewTempB = fragmentB.getView().findViewById(R.id.text_view_fragment_b);
        textViewTempB.setText(fragmentB.getContadorFragmentB()+"");
    }
}
