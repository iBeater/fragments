package com.example.amartineza.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentC extends Fragment {

    Button buttonRestar;
    interfazRestar interfazRestar;

    public FragmentC() {
        // Required empty public constructor
    }

    public static FragmentC newInstance() {
        FragmentC fragment = new FragmentC();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_c, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonRestar = view.findViewById(R.id.button_fragment_c);

        buttonRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interfazRestar.restar();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            interfazRestar = (interfazRestar) context;
        }catch (ClassCastException e ){
            throw  new ClassCastException(context.toString() + "debe implementar interfazFragmentC");
        }
    }

    public interface interfazRestar{
        void restar();
    }
}
