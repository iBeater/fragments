package com.example.amartineza.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    TextView textViewFragmentB;
    Button buttonFragmentB;
    EnviarMensaje enviarMensaje;

    private int contadorFragmentB = 0;

    public int getContadorFragmentB() {
        return contadorFragmentB;
    }

    public void setContadorFragmentB(int contadorFragmentB) {
        this.contadorFragmentB = contadorFragmentB;
    }

    public FragmentB() {
        // Required empty public constructor
    }

    public static FragmentB newInstance() {
        FragmentB fragment = new FragmentB();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_b, container, false);
        // Inflate the layout for this fragment
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textViewFragmentB = view.findViewById(R.id.text_view_fragment_b);
        buttonFragmentB = view.findViewById(R.id.button_fragment_b);

        buttonFragmentB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contadorFragmentB++;
                enviarMensaje.enviarSaludoFragment("Hola Fragment B", contadorFragmentB, 2);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            enviarMensaje = (EnviarMensaje) context;
        }catch (ClassCastException e ){
            throw  new ClassCastException(context.toString() + "debe implementar interfazFragmentB");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
