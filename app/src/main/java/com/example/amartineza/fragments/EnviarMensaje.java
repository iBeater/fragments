package com.example.amartineza.fragments;

/**
 * Created by amartineza on 3/7/2018.
 */

public interface EnviarMensaje {
    void enviarSaludoFragment(String mensaje, int contador, int id);
}
