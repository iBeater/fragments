package com.example.amartineza.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class FragmentA extends Fragment {

    TextView textViewFragmentA;
    Button buttonFragmentA;
    EnviarMensaje enviarMensaje;

    private int contadorFragmentA = 0;

    public int getContadorFragmentA() {
        return contadorFragmentA;
    }

    public void setContadorFragmentA(int contadorFragmentA) {
        this.contadorFragmentA = contadorFragmentA;
    }

    public FragmentA() {
        // Required empty public constructor
    }

    public static FragmentA newInstance() {
        FragmentA fragment = new FragmentA();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_a, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
       super.onViewCreated(view, savedInstanceState);
        textViewFragmentA = view.findViewById(R.id.text_view_fragment_a);
        buttonFragmentA = view.findViewById(R.id.button_fragment_a);

        buttonFragmentA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contadorFragmentA++;
                enviarMensaje.enviarSaludoFragment("Hola Fragment A", contadorFragmentA, 1);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            enviarMensaje = (EnviarMensaje) context;
        }catch (ClassCastException e ){
            throw  new ClassCastException(context.toString() + "debe implementar interfazFragmentA");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
